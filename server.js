require( 'dotenv' ).config();
const express = require( 'express' );
const cors = require( 'cors' );

const app = express();

app.use( cors( { optionsSuccessStatus: 200 } ) );

app.use( express.static( 'public' ) );

app.get( '/', ( req, res ) => {
    res.sendFile( __dirname + '/views/index.html' )
} );

app.get( '/api/timestamp/:date_string?', ( req, res ) => {
    var newDate = req.params.date_string
                  ? new Date( req.params.date_string )
                  : new Date();

    if ( isNaN( newDate ) ) {
        res.json( { "unix": null, "utc": "Invalid Date" } );
    } else {
        res.json( { "unix": newDate.getTime(), "utc": newDate.toUTCString() } );
    }
} );

var listener = app.listen( process.env.PORT, () => {
    console.log( 'Listening on port ' + process.env.PORT )
} );
